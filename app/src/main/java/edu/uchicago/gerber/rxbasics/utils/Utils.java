package edu.uchicago.gerber.rxbasics.utils;

import java.util.Collection;
import java.util.stream.Collectors;

public class Utils {

    public static String toJokeString(Collection<String> strings){
        return strings
                .stream()
                .collect(Collectors.joining("\n\n"));

    }
}
