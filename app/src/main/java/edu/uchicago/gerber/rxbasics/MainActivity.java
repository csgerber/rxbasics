package edu.uchicago.gerber.rxbasics;

import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {


    private MainViewModel viewModel ;
    private TextView tv_single_joke;
    private Button btn_single_joke;


    //implement laziness
    public synchronized MainViewModel getViewModel() {
        if (viewModel == null) {
            viewModel = new MainViewModel();
        }
        return viewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv_single_joke = findViewById(R.id.tv_single_joke);
        btn_single_joke = findViewById(R.id.btn_single_joke);
        btn_single_joke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getViewModel().onJokesRequest();
            }
        });


        observeViewModel();
    }

    private void observeViewModel() {

        getViewModel().getJokes().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
               tv_single_joke.setText(null != s ? s: "");
            }
        });



    }
}
