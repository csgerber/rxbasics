
package edu.uchicago.gerber.rxbasics.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

// http://api.icndb.com/jokes/random
//@Generated("jsonschema2pojo")
public class JokeResponse {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("value")
    @Expose
    private Value joke;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Value getJoke() {
        return joke;
    }

    public void setJoke(Value joke) {
        this.joke = joke;
    }

}
