package edu.uchicago.gerber.rxbasics;

import android.annotation.SuppressLint;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;
import java.util.stream.Collectors;

import edu.uchicago.gerber.rxbasics.models.JokeResponse;
import edu.uchicago.gerber.rxbasics.models.JokesResponse;
import edu.uchicago.gerber.rxbasics.models.ValueList;
import io.reactivex.Single;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class MainViewModel  extends ViewModel {


    private MutableLiveData<String> _joke, _jokes;
    private LiveData<String> joke, jokes;

    public MainViewModel() {

        _joke = new MutableLiveData<>();
        joke = _joke;

        _jokes = new MutableLiveData<>();
        jokes = _jokes;
    }

    //lazy
    private JokeApiService jokeApiService;
    //ensure that no two threads can call this method simultaneously - overkill
    private synchronized  JokeApiService getJokeApiService(){
        if (null == jokeApiService){
           jokeApiService = JokeApiServiceFactory.createService();
        }
        return jokeApiService;
    }

    //lazy 
    private Single<JokeResponse> singleRandomJoke;
    //ensure that no two threads can call this method simultaneously
    private synchronized  Single<JokeResponse>  getSingleRandomJoke(){
        if (null == singleRandomJoke){
         singleRandomJoke = getJokeApiService().randomJoke();
        }
        return singleRandomJoke;
    }


    //lazy
    private Single<JokesResponse> lotsJokes;
    //ensure that no two threads can call this method simultaneously
    private synchronized  Single<JokesResponse>  getLotsJokes(){
        if (null == lotsJokes){
            lotsJokes = getJokeApiService().lotsOfJokes();
        }
        return lotsJokes;
    }


    public LiveData<String> getJoke() {
        return joke;
    }

    public LiveData<String> getJokes() {
        return jokes;
    }

    @SuppressLint("CheckResult")
    public void onJokeRequest() {
        getSingleRandomJoke().subscribeOn(Schedulers.io())
                .subscribe(new DisposableSingleObserver<JokeResponse>() {
                    @Override
                    public void onSuccess(@NonNull JokeResponse jokeResponse) {
                        _joke.postValue(jokeResponse.getJoke().getJoke());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        _joke.postValue("error");
                    }
                });


    }

    @SuppressLint("CheckResult")
    public void onJokesRequest() {
        getLotsJokes().subscribeOn(Schedulers.io())
                .subscribe(new DisposableSingleObserver<JokesResponse>() {
                    @Override
                    public void onSuccess(@NonNull JokesResponse jokesResponse) {
                        _jokes.postValue(convertListToString(jokesResponse.getValue()));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        _jokes.postValue("error");
                    }
                });


    }

    public String convertListToString(List<ValueList> list){

        StringBuilder stringBuilder = new StringBuilder();
        for (ValueList val : list) {
            stringBuilder.append(val.getJoke());
            stringBuilder.append("\n\n");
        }
        return stringBuilder.toString();
    }



    
}
