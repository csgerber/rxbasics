
package edu.uchicago.gerber.rxbasics.models;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class JokesResponse {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("value")
    @Expose
    private List<ValueList> valueList = null;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<ValueList> getValue() {
        return valueList;
    }

    public void setValue(List<ValueList> value) {
        this.valueList = value;
    }

}
