package edu.uchicago.gerber.rxbasics;

import edu.uchicago.gerber.rxbasics.models.JokeResponse;
import edu.uchicago.gerber.rxbasics.models.JokesResponse;
import io.reactivex.Single;
import retrofit2.http.GET;

public interface JokeApiService {

    @GET("jokes/random")
    Single<JokeResponse> randomJoke();


    @GET("jokes")
    Single<JokesResponse> lotsOfJokes();

}
